---
template: no_toc.html
---

#### 2024-02-10 [Keycloak SSO with docker compose and nginx](blog/2024-02-10_keycloak-docker-compose-nginx.md)

___

#### 2023-11-12 [Mastodon with docker rootless, compose, and nginx reverse proxy](blog/2023-12-12_mastodon-docker-rootless.md)

___

#### 2022-11-12 [IPSEC between OPNsense and pfSense](blog/2021-11-19_pfsense_opnsense_ipsec_cgnat.md)

#### 2022-08-04 [Proxmox root migration (a report)](blog/2022-01-21_proxmox_root_migration)

___

#### 2021-12-05 [Flashing the Supermicro variant of SAS9201-16e](blog/2021-12-05_supermicro_sas2116_flashing.md)

#### 2021-05-05 [Proxmox Hypervisor Monitoring with Telegraf and InfluxDB](blog/2021-05-05_proxmox_influxdb.md)

#### 2021-04-10 [Building SnapOS Image for Raspberry Pi Zero W with buildroot in WSL2](blog/2021-04-10_buildroot-snapos.md)

#### 2021-03-25 [Running docker inside an unprivileged LXC container on Proxmox](blog/2021-03-25_proxmox_docker.md)
