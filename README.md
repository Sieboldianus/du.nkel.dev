<a href="http://sourcefoundry.org/cinder/"><img src="img/header.png" alt="Cinder | A clean, responsive theme for MkDocs" width="728"></a>

## du.nkel.dev mkdocs

A clean, responsive MkDocs static documentation site generator theme

[Demo and Documentation](http://sourcefoundry.org/cinder/)

## Manual Built

```bash
conda activate mkdocs-cinder
mkdocs serve
# or
mkdocs build
```

## Setup

```bash
conda create -n mkdocs-cinder
conda activate mkdocs-cinder
conda install -c conda-forge mkdocs pygments pymdown-extensions
pip install mkdocs-git-revision-date-plugin mkdocs-rss-plugin
```
